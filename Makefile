#
# This is a project Makefile. It is assumed the directory this Makefile resides in is a
# project subdirectory.
#

PROJECT_NAME := tmpl
	
EXTRA_COMPONENT_DIRS += \
$(PROJECT_PATH)/../esp32-core 

COMPONENTS=\
main \
partition_table\
pthread \
freertos \
esp32 \
ethernet \
newlib \
esptool_py \
nvs_flash \
spi_flash \
log \
tcpip_adapter \
lwip \
xtensa-debug-module \
driver \
soc \
bootloader \
bootloader_support \
heap \
app_trace \
mbedtls \
vfs \
wpa_supplicant \
micro-ecc \
json \
app_update \
console \
n2g \
ota \
scop \
grip \
util \
wlan \
web \
zcl \
nwk \
meter \
led \
cli
	
CFLAGS+= -I$(PROJECT_PATH)/../esp32-core/grip \
-I$(PROJECT_PATH)/../esp32-core/n2g \
-I$(PROJECT_PATH)/../esp32-core/ota \
-I$(PROJECT_PATH)/../esp32-core/scop \
-I$(PROJECT_PATH)/../esp32-core/util \
-I$(PROJECT_PATH)/../esp32-core/wlan \
-I$(PROJECT_PATH)/../esp32-core/web \
-I$(PROJECT_PATH)/../esp32-core/zcl \
-I$(PROJECT_PATH)/../esp32-core/nwk\
-I$(PROJECT_PATH)/../esp32-core/meter \
-I$(PROJECT_PATH)/../esp32-core/led \
-I$(PROJECT_PATH)/../esp32-core/cli \
-I$(PROJECT_PATH)/main 

include $(IDF_PATH)/make/project.mk
