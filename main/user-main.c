/*
 * user-main.c
 * 
 * main entry point into user code
 * 
 * Copyright 2017 Net2Grid. All rights reserved 
 */

#include "n2g-common.h"
#include "rest.h"
#include "n2g-access.h"
#include "n2g-session.h"
#include "zcl-reporting.h"
#include "sys-time.h"
#include "dbg.h"
#include "utils.h"
#include "nvs.h"
#include "nvs_flash.h"
#include "esp_partition.h"
#include "esp_ota_ops.h"
#include "ota.h"
#include "wlan-api.h"
#include "network.h"
#include "driver/gpio.h"
#include "driver/i2c.h"
#include "esp_wifi.h"
#include "esp_log.h"
#include "cli.h"
#include "button.h"
#include "system-cli.h"
#include "nwk-cli.h"
#include "wlan-cli.h"
#include "util-cli.h"
#include "zcl-cli.h"
#include "zcl-basic-server.h"

#define DBG printf

#define TIME_SYNC_TIMEOUT_SEC  300
#define MAX_KEEPALIVE_WAIT_SEC 90
#define TERMINATION_DELAY_MIN  30
#define HARDWARE_VERSION 1

#define APP_TAG "TMPL_APP"

#define OTA_RETRY_TIMEOUT_SEC 10800
#define N2G_RETRY_TIMEOUT_SEC 30

#define FREE_HEAP_SZ_MIN 2048

#define OPT_IN_NVS_KEY  "opt_in"
#define APP_NVS_NAMESPACE "app"
#define BOOT_CTR_NVS_KEY  "boot_ctr"

static void button_evt(button_data_t *button);
static void wlan_register_cmd(void);
int checkFirstBoot();

const uint8_t n2g_prefix[4] = {0x84, 0xDF, 0x0C, 0x10};

const device_info_t dev_info = {DEVICE_INFO_TAG,
  "NET2GRID\0",
  "TMPL0000\0",
  HARDWARE_VERSION,
  { 0x00, 0x00, 0x01},
  0x4E47,
  0x3302};

static bool wait_read = 1;

static uint32_t boot_ctr = 0;

static uint16_t ota_retry_counter = 1;

static char *date_code = NULL;

/*
 *	increment boot ctr
 */
static void increment_boot_counter(void)
{
  nvs_handle hnvs;
  esp_err_t err;
  bool opt_in;

  boot_ctr = 0;

  err = nvs_open(APP_NVS_NAMESPACE, NVS_READWRITE, &hnvs);
  if (err != ESP_OK) {
    return;
  }

  // value will default to 0, if not set yet in NVS
  err = nvs_get_u32(hnvs, BOOT_CTR_NVS_KEY, &boot_ctr);

  if (err == ESP_OK || err == ESP_ERR_NVS_NOT_FOUND) {
    // Write
    boot_ctr++;
    nvs_set_u32(hnvs, BOOT_CTR_NVS_KEY, boot_ctr);
    nvs_commit(hnvs);
  }
#ifdef PLATFORM_OPT_IN
  // Read opt-in status
  err = nvs_get_u8(hnvs, OPT_IN_NVS_KEY, &opt_in);
  if(err == ESP_OK){
    zcl_basic_server_set_platform_opt_in(opt_in?1:0);
  }
#endif
  // Close
  nvs_close(hnvs);
}

/*
 *	return boot ctr
 */
uint32_t get_boot_counter(void)
{
  return boot_ctr;
}

void ota_check(void)
{
  ota_retry_counter = 3;
}

static void app_ota_check(void)
{
  if (ota_retry_counter == 0) {
    ota_init();
    ota_retry_counter = OTA_RETRY_TIMEOUT_SEC;
  }

  ota_retry_counter--;
}

static void app_n2g_check(void)
{
  static uint16_t retry_counter = 3;

  if (ota_get_count() == 0) {
    return;
  }

  if (n2g_access_session_is_idle()) {
    if (retry_counter == 0) {
      if (retry_counter-- == 0) {
        LED_PLATFORM_WAIT();
        n2g_session_init();
        retry_counter = N2G_RETRY_TIMEOUT_SEC;
      }
    }
    retry_counter--;
  }
}

/*
 *	main tick
 */
void main_task(void *pvParameters)
{
  time_t t;
  while (1) {
    if (nwk_state_got_ip()) {
      app_ota_check();
      app_n2g_check();
    }

    time(&t);
    if ((t & 0x3F) == 0x20) {
      ESP_LOGV(SYS_TIME_TAG, "%d", (int) t);
    }

    
    if ((n2g_get_last_keepalive_time() != 0) &&
            (t - n2g_get_last_keepalive_time() > 120)) {
      if(!n2g_access_session_is_idle()){
        n2g_terminate_session();
      }
    }
    
    if(ota_get_count() != 0){
      nwk_ip_monitor();
    }

    if (ota_get_count() != 0) {
      if (esp_get_free_heap_size() < FREE_HEAP_SZ_MIN) {
        ESP_LOGE("MEM", "out of mem");
        esp_restart();
      }
    }
    vTaskDelay(1000/portTICK_PERIOD_MS);
  }
}

/*
 *	main init
 */
void main_init(void)
{
  xTaskCreate(main_task, (const char*) "main", 4096, NULL, tskIDLE_PRIORITY + 1, NULL);
}

char *get_custom_ap_key(void)
{
  return NULL;
}

void app_main(void)
{
  char *info_str = NULL;
  esp_err_t err = nvs_flash_init();

  if (err == ESP_ERR_NVS_NO_FREE_PAGES) {
    // OTA app partition table has a smaller NVS partition size than the non-OTA
    // partition table. This size mismatch may cause NVS initialization to fail.
    // If this happens, we erase NVS partition and initialize NVS again.
    const esp_partition_t* nvs_partition;
    nvs_partition = esp_partition_find_first(ESP_PARTITION_TYPE_DATA,
                                             ESP_PARTITION_SUBTYPE_DATA_NVS,
                                             NULL);
    esp_partition_erase_range(nvs_partition, 0, nvs_partition->size);
    err = nvs_flash_init();
  }
  printf("NVS: init %d\n", err);

  //button_init(&button_evt);
  get_json_device_info(&info_str);
  if (info_str) {
	  printf("%s\n", info_str);
	  free(info_str);
	  fflush(stdout);
  }

  esp_partition_t* prt;

  prt = esp_ota_get_running_partition();
    ESP_LOGV("APP", "partition %s encryption %sabled",
             prt->label,
             prt->encrypted?"en":"dis");

  increment_boot_counter();

  //led_init();

  nwk_state_init();

  sys_time_init();
  main_init();
  system_cli_init();

  wlan_register_cmd();

  util_cli_init();
  
  zb_ep_activate_all();
  zcl_reporting_init(NULL);

  cli_init("TMPL>");

}

/*
 *	get device info
 */
device_info_t* get_dev_info(void)
{
  return (device_info_t*)&dev_info;
}

/*
 *	get device short ID
 */
void
get_device_short_id(uint8_t* short_id)
{
  uint8_t mac[6];
  uint32_t integer_id = 0;

  esp_efuse_mac_get_default(mac);

  integer_id = mac[3] << 16;
  integer_id += mac[4] << 8;
  integer_id += mac[5];

  base56_encode(integer_id, short_id);
}

/*
 *	get device long ID
 */
boolean_t
get_device_long_id(uint8_t* long_id)
{
  uint8_t mac[6];
  uint8_t i, k;

  esp_efuse_mac_get_default(mac);

  for (i = 0, k = 0; i < 8; i++) {
    if (i == 3 || i == 4) {
      long_id[i] = 0;
    } else {
      long_id[i] = mac[k++];
    }
  }

  return true;
}

/*
 *	button event callback
 */
static void button_evt(button_data_t *button)
{
  if (button->state == BUTTON_PRESSED) {
    printf("push\n");
  } else {
    printf("rls\n");
  }
}


static int wlan(int argc, char** argv)
{
  printf("init wlan\n");
  nwk_state_init();
  return 0;
}

static void wlan_register_cmd(void)
{
  static const esp_console_cmd_t wlan_cmd = {
    .command = "wlan",
    .help = "wlan init",
    .hint = NULL,
    .func = &wlan
  };
  ESP_ERROR_CHECK(esp_console_cmd_register(&wlan_cmd));
}


/*
 *	return JSON formatted device info
 */
boolean_t get_json_device_info(char** result)
{
  cJSON *root;
  uint8_t tmp_str[32];
  uint8_t eui[8];
  device_info_t* pdev_info;

  root = cJSON_CreateObject();
  *result = NULL;

  if (root != NULL) {

    pdev_info = get_dev_info();

    get_device_long_id(eui);
    sprintf((char*) tmp_str,
            "%02x%02x:%02x%02x:%02x%02x:%02x%02x",
            eui[0],
            eui[1],
            eui[2],
            eui[3],
            eui[4],
            eui[5],
            eui[6],
            eui[7]);
    cJSON_AddStringToObject(root, "id", (char*) tmp_str);

    cJSON_AddStringToObject(root, "mf", (char*) pdev_info->mf_name);
    cJSON_AddStringToObject(root, "model", (char*) pdev_info->dev_name);

    sprintf((char*) tmp_str,
            "%d.%d.%d",
            pdev_info->fw_version[0],
            pdev_info->fw_version[1],
            pdev_info->fw_version[2]);
    cJSON_AddStringToObject(root, "fw", (char*) tmp_str);

    cJSON_AddStringToObject(root, "hw", "0.3");
    cJSON_AddStringToObject(root, "batch", "-");

    *result = cJSON_PrintUnformatted(root);
    cJSON_Delete(root);
  }

  return (*result != NULL);
}

// return -1 on fail 0 on first boot and 1 if not first boot
int checkFirstBoot()
{
  nvs_handle nvsHandler;
  esp_err_t err = nvs_open("nvs", NVS_READWRITE, &nvsHandler);
  if (err != ESP_OK) {
    ESP_LOGI(APP_TAG, "Unable to open NVS");
    return -1;
  } else {
    uint8_t firstBoot = 0;
    err = nvs_get_u8(nvsHandler, "firstBoot", &firstBoot);
    if (err != ESP_OK) {
      if (err == ESP_ERR_NVS_NOT_FOUND) {
        ESP_LOGI(APP_TAG, "First boot. Setting default values");
        err = nvs_set_u8(nvsHandler, "firstBoot", 1);
        if (err != ESP_OK) {
          ESP_LOGI(APP_TAG, "Failed to set first boot value");
          nvs_close(nvsHandler);
          return -1;
        }
        err = nvs_commit(nvsHandler);
        if (err != ESP_OK) {
          ESP_LOGI(APP_TAG, "Failed to set first boot value");
          nvs_close(nvsHandler);
          return -1;
        }
      } else {
        ESP_LOGI(APP_TAG, "NVS error : %04X", err);
        nvs_close(nvsHandler);
        return -1;
      }
    } else {
      ESP_LOGI(APP_TAG, "Not in first boot. Read values from memory");
      nvs_close(nvsHandler);
      return 1;
    }
  }
  nvs_close(nvsHandler);
  return 0;
}

/*
 *	store opt in
 */
void store_opt_in(bool opt_in)
{
  nvs_handle hnvs;
  esp_err_t err;

  err = nvs_open(APP_NVS_NAMESPACE, NVS_READWRITE, &hnvs);
  if (err != ESP_OK) {
    return;
  }

  // Store opt-in status
  err = nvs_set_u8(hnvs, OPT_IN_NVS_KEY, opt_in);
  if(err == ESP_OK){
    nvs_commit(hnvs);
  }

  // Close
  nvs_close(hnvs);
}

char *get_date_code(void)
{
  return date_code;
}
