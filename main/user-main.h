/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   user-main.h
 * Author: sjm
 *
 * Created on 15 June 2018, 14:43
 */

#ifndef USER_MAIN_H
#define USER_MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

device_info_t* get_dev_info(void);
void get_device_short_id(uint8_t* short_id);
boolean_t get_device_long_id(uint8_t* long_id);
uint32_t get_boot_counter(void);
boolean_t get_json_device_info(char** result);
char *get_date_code(void);
void ota_check(void);
char *get_custom_ap_key(void);
void store_opt_in(bool opt_in);

#ifdef __cplusplus
}
#endif

#endif /* USER_MAIN_H */

