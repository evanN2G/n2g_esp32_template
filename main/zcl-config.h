/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   zcl-config.h
 * Author: sjm
 *
 * Created on 30 April 2018, 09:58
 */

#ifndef ZCL_CONFIG_H
#define ZCL_CONFIG_H

#include "zb-endpoint.h"

#ifdef __cplusplus
extern "C" {
#endif

// ENDPOINT 1
#define CONFIG_EP                    1
#define ZGD_MAIN_EP                  CONFIG_EP
#define ZGD_EP1_PROFILE_ID           HA_PROFILE_ID
#define ZGD_EP1_DEVICE_ID            ZCL_DEVICE_ID_COMBINED_INTERFACE
#define ZGD_EP1_DEVICE_VERSION       0
#define ZGD_EP1_SERVER_CL_COUNT      (sizeof(ep1_server_cluster_list)/sizeof(*ep1_server_cluster_list))
#define ZGD_EP1_CLIENT_CL_COUNT      0

#define ZCL_REPORT_CONFIG {\
}

#define ZCL_CLUSTER_LIST \
static const zb_cluster_t* ep1_server_cluster_list[] = {\
  &basic_server_cluster,\
  &lan_config_server_cluster\
};

#define ZB_ENDPOINT_TABLE \
static const zb_endpoint_t zb_ep_table[] = {\
  { ZGD_MAIN_EP, 0, ZGD_EP1_PROFILE_ID, ZGD_EP1_DEVICE_ID, ZGD_EP1_DEVICE_VERSION, ZGD_EP1_SERVER_CL_COUNT, ZGD_EP1_CLIENT_CL_COUNT, (zb_cluster_t**) ep1_server_cluster_list, NULL},\
};

#define ZGD_STATIC_EP_COUNT             (sizeof(zb_ep_table)/sizeof(*zb_ep_table))

#ifdef __cplusplus
}
#endif

#endif /* ZCL_CONFIG_H */

