/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   n2g-common.h
 * Author: sjm
 *
 * Created on 23 June 2017, 14:52
 */

#ifndef APP_CONFIG_H
#define APP_CONFIG_H

#include "board.h"
#include "led.h"
#include "zcl-config.h"

#ifdef __cplusplus
extern "C" {
#endif

#define HOSTNAME "tmpl"
#define HARDWARE_VERSION 1

#define LED_PLATFORM_OK()
#define LED_PLATFORM_WAIT()
#define LED_WAIT_IP()
#define LED_GOT_IP()
#define LED_WPS()
#define LED_NWK_ERR()  
#define LED_AP_UP()
  
#define UID_PREFIX "tmpl-"

//#define LPHY_TX_BLOCKING
//#define LMAC_NO_NVS

//#define LORA_CLI
//#define SX1272
//#define LORA_SX1272_INIT_DEFAULTS

#define PLATFORM_OPT_IN
#define PLATFORM_OPT_IN_DEFAULT 1

#define LAN_API_TOKEN_AUTH

#ifdef __cplusplus
}
#endif

#endif /* APP_CONFIG_H */

