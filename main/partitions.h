/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   partitions.h
 * Author: sjm
 *
 * Created on 19 June 2018, 13:29
 */

#ifndef PARTITIONS_H
#define PARTITIONS_H

#ifdef __cplusplus
extern "C" {
#endif

#define OTA0_PARTITION_OFFSET   0x10000
#define OTA1_PARTITION_OFFSET   0x130000


#ifdef __cplusplus
}
#endif

#endif /* PARTITIONS_H */

