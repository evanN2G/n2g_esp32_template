/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   dbg.h
 * Author: sjm
 *
 * Created on 24 April 2018, 14:13
 */

#ifndef DBG_H
#define DBG_H

#ifdef __cplusplus
extern "C" {
#endif

#define NETWORK_LOG_LEVEL      ESP_LOG_VERBOSE
#define ASN1_LOG_LEVEL         ESP_LOG_INFO
#define GRIP_LOG_LEVEL         ESP_LOG_VERBOSE
#define SCOP_LOG_LEVEL         ESP_LOG_INFO
#define HTTP_SERVER_LOG_LEVEL  ESP_LOG_VERBOSE
#define HTTP_CLIENT_LOG_LEVEL  ESP_LOG_INFO
#define WLAN_LOG_LEVEL         ESP_LOG_VERBOSE
#define ZB_ENDPOINT_LOG_LEVEL  ESP_LOG_VERBOSE
#define ZCL_REPORT_LOG_LEVEL   ESP_LOG_INFO
#define AES_CCM_LOG_LEVEL      ESP_LOG_INFO
#define N2G_LOG_LEVEL          ESP_LOG_VERBOSE
#define DNS_SD_LOG_LEVEL       ESP_LOG_INFO
#define OTA_LOG_LEVEL          ESP_LOG_VERBOSE
#define HOSTNAME_LOG_LEVEL     ESP_LOG_INFO
#define TLS_LOG_LEVEL          ESP_LOG_INFO
#define MEDA_LOG_LEVEL         ESP_LOG_INFO
#define METER_LOG_LEVEL        ESP_LOG_INFO
#define BUTTON_LOG_LEVEL       ESP_LOG_VERBOSE
#define LORA_LOG_LEVEL         ESP_LOG_VERBOSE
#define LMAC_LOG_LEVEL         ESP_LOG_VERBOSE
#define LPHY_LOG_LEVEL         ESP_LOG_VERBOSE


#ifdef __cplusplus
}
#endif

#endif /* DBG_H */

